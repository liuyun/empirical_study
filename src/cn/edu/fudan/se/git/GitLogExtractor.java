package cn.edu.fudan.se.git;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.edu.fudan.se.constant.MySqlConstants;

public class GitLogExtractor {
	public String project;
	private Connection conn;
	private PreparedStatement pstmt1;
	private PreparedStatement pstmt2;

	public GitLogExtractor(String project) {
		this.project = project;
		try {
			Class.forName(MySqlConstants.DRIVER);
			conn = DriverManager.getConnection(MySqlConstants.URL, MySqlConstants.USER, MySqlConstants.PASSWORD);
			String sql1 = "insert into commit (project, commit_id, author, email, time, message, revision) values (?,?,?,?,?,?,?)";
			String sql2 = "insert into file (project, commit_id, file_name, type, time, path) values (?,?,?,?,?,?)";
			pstmt1 = conn.prepareStatement(sql1);
			pstmt2 = conn.prepareStatement(sql2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void gitCommand(String gitHome, String cmd, String dir) {
		try {
			Process process = Runtime.getRuntime().exec(gitHome + cmd, null, new File(dir));
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF-8"));
			FileWriter fw = new FileWriter(new File("C:\\Users\\echo\\Desktop\\" + project + ".log"));

			String line = null;
			while ((line = reader.readLine()) != null) {
				fw.write(line + "\n");
			}

			fw.flush();
			fw.close();
			process.getOutputStream().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void parseGitLog(String log) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(log), "UTF-8"));
			String line = null;
			String txt = null;

			while ((line = reader.readLine()) != null) {
				if (!line.matches("\\s*"))
					if (line.startsWith("commit ")) {
						parseCommitInfo(txt);
						txt = line.trim() + "\n";
					} else {
						txt += line.trim() + "\n";
					}
			}
			reader.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void parseCommitInfo(String txt) {
//		System.out.println(txt);
		if (txt == null)
			return;
		String commitId = null;
		String author = "";
		String email = null;
		String dateStr = null;
		Timestamp time = null;
		String message = "";
		String revision = null;
		List<String> fileNames = new ArrayList<String>();
		List<String> types = new ArrayList<String>();

		String[] lines = txt.split("\n");
		for (String line : lines) {
			if (line.startsWith("commit ")) {
				commitId = line.split("\\s+")[1];
			} else if (line.startsWith("Author:")) {
				String[] tokens = line.split("\\s+");
				for (int i = 1; i < tokens.length; i++) {
					if (i == tokens.length - 1) {
						email = tokens[i];
					} else {
						author += tokens[i] + " ";
					}
				}
			} else if (line.startsWith("Date:")) {
				dateStr = line.substring(5).trim();
				Date date = new Date(dateStr);
				time = new Timestamp(date.getTime());

			} else if (line.startsWith("M	") || line.startsWith("A	") || line.startsWith("D	")) {
				types.add(line.split("\\s+")[0]);
				fileNames.add(line.split("\\s+")[1]);
			} else if (!line.startsWith("Merge:") && !line.matches("\\s*")) {
				message += line + ";";
			}
		}

		Timestamp revision1 = Timestamp.valueOf("2016-06-27 03:33:31");

		if (time.compareTo(revision1) <= 0) {
			revision = "empty";
		}

		if (revision != null) {
			try {
				pstmt1.setString(1, project);
				pstmt1.setString(2, commitId.trim());
				pstmt1.setString(3, author.trim());
				pstmt1.setString(4, email.trim());
				pstmt1.setTimestamp(5, time);
				pstmt1.setString(6, message.trim());
				pstmt1.setString(7, revision);
				pstmt1.executeUpdate();
				// System.out.println(pstmt1);
				storeFiles(commitId, fileNames, types, time);

			} catch (SQLException e) {
				System.out.println(pstmt1);
				e.printStackTrace();
			}
		}
	}

	public void storeFiles(String commitId, List<String> fileNames, List<String> types, Timestamp time) {
		for (int i = 0; i < fileNames.size(); i++) {
			String fileName = fileNames.get(i);
			if (!fileName.contains("/test/") && fileName.endsWith(".java")) {
				try {
					pstmt2.setString(1, project);
					pstmt2.setString(2, commitId);
					pstmt2.setString(3, parseName(fileName));
					pstmt2.setString(4, types.get(i));
					pstmt2.setTimestamp(5, time);
					pstmt2.setString(6, fileName);
					pstmt2.executeUpdate();
					// System.out.println(pstmt2);
				} catch (SQLException e) {
					System.out.println(pstmt2);
					e.printStackTrace();
				}
			}
		}
	}

	public String parseName(String fileName) {
		// System.out.println(fileName);
		return fileName.replaceAll(".java", "_java").replaceAll("/", ".");
	}
}
