package cn.edu.fudan.se.git;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffEntry.ChangeType;
import org.eclipse.jgit.errors.AmbiguousObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.errors.RevisionSyntaxException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;

import cn.edu.fudan.se.constant.MySqlConstants;

public class GitExtractor {
	private Repository repo;
	private String project;
	private Git git;
	private RevWalk walk;
	Connection conn = null;
	Statement stmt;

	public GitExtractor(String gitRepoPath, String project) {
		this.project = project;
		try {
			git = Git.open(new File(gitRepoPath));
			repo = git.getRepository();
			walk = new RevWalk(repo);

			try {
				Class.forName(MySqlConstants.DRIVER);
				conn = DriverManager.getConnection(MySqlConstants.URL, MySqlConstants.USER, MySqlConstants.PASSWORD);
				stmt = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// extract commits from begin to end
	public void track(Timestamp begin, Timestamp end, String prevVersion, String version)
			throws RevisionSyntaxException, NoHeadException, MissingObjectException, IncorrectObjectTypeException,
			AmbiguousObjectException, GitAPIException, IOException {
		for (RevCommit commit : git.log().all().call()) {
			extractCommit(commit, begin, end, prevVersion, version);
		}
		walk.dispose();
	}

	// extract commits that the number of co-change files >= 2
	public void extractCommit(RevCommit commit, Timestamp begin, Timestamp end, String prevVersion, String version)
			throws RevisionSyntaxException, MissingObjectException, IncorrectObjectTypeException,
			AmbiguousObjectException, IOException, GitAPIException {
		String commitId = null;
		if (commit == null || (commitId = commit.getName()) == null) {
			return;
		}

		TreeWalk treeWalk = new TreeWalk(repo);
		RevTree tree = commit.getTree();
		treeWalk.addTree(tree);
		treeWalk.setRecursive(true);

		long time = commit.getCommitTime();
		time = time * 1000;
		Timestamp commitTime = new Timestamp(time);

		// extract commits from begin to end
		if (commitTime.compareTo(begin) > 0 && commitTime.compareTo(end) <= 0) {
			String author = commit.getAuthorIdent().getName();
			String committer = commit.getCommitterIdent().getName();
			String message = commit.getShortMessage().replace("\"", "\\\"");

			// exclude merge commit
			// if (!message.toLowerCase().contains("merge")) {
			RevCommit prevCommit = null;
			if (commit.getParentCount() > 0) {
				prevCommit = commit.getParent(0);
				int count = extractChange(commit, prevCommit, commitId);
				if (count >= 2) {
					String sql = "insert into commit values(\"" + project + "\",\"" + commitId + "\",\""
							+ prevCommit.getName() + "\",\"" + author + "\",\"" + commitTime + "\",\"" + committer
							+ "\",\"" + message + "\",\"" + version + "\",\"" + prevVersion + "\")";
					try {
						// System.out.println(sql);
						stmt.execute(sql);
					} catch (Exception e) {
						System.out.println(sql);
						e.printStackTrace();
					}

				}
			}
			// }
		}
	}

	// extract co-change files, exclude test file and non-java file
	public int extractChange(RevCommit commit, RevCommit prevTargetCommit, String commitId)
			throws IncorrectObjectTypeException, IOException, GitAPIException {
		int count = 0;
		AbstractTreeIterator currentTreeParser = prepareTreeParser(commit.getName());
		AbstractTreeIterator prevTreeParser = prepareTreeParser(prevTargetCommit.getName());
		List<DiffEntry> diffs = git.diff().setNewTree(currentTreeParser).setOldTree(prevTreeParser).call();
		List<String> sqls = new ArrayList<String>();
		for (DiffEntry diff : diffs) {
			String newPath = diff.getNewPath();
			String oldPath = diff.getOldPath();

			String fileName = null;
			if (ChangeType.DELETE.name().equals((diff.getChangeType().name()))) {
				fileName = oldPath;
			} else if (newPath != null) {
				fileName = newPath;
			}
			if (fileName == null) {
				continue;
			}

			long time = commit.getCommitTime();
			time = time * 1000;
			// exclude test and non-java file
			// if (fileName.startsWith("src/java") && fileName.endsWith(".java")
			// && !diff.getChangeType().name().equals("DELETE")) {
			if (fileName.startsWith("src/java") && fileName.endsWith(".java")) {
				sqls.add("insert into file values(\"" + project + "\",\"" + commitId + "\",\"" + parseName(fileName)
						+ "\",\"" + diff.getChangeType().name() + "\",\"" + parseName(newPath) + "\",\""
						+ parseName(oldPath) + "\",\"" + new Timestamp(time) + "\")");
				count++;
			}
		}
		if (count >= 2) {
			for (String sql : sqls) {
				try {
					stmt.execute(sql);
				} catch (Exception e) {
					System.out.println(sql);
					e.printStackTrace();
				}
			}
		}
		return count;
	}

	public CanonicalTreeParser prepareTreeParser(String objectId)
			throws IOException, MissingObjectException, IncorrectObjectTypeException {
		// from the commit we can build the tree which allows us to construct
		// the TreeParser
		RevWalk walk = new RevWalk(repo);
		RevCommit commit = walk.parseCommit(ObjectId.fromString(objectId));
		RevTree tree = walk.parseTree(commit.getTree().getId());

		CanonicalTreeParser treeParser = new CanonicalTreeParser();
		ObjectReader oldReader = repo.newObjectReader();
		treeParser.reset(oldReader, tree.getId());
		walk.dispose();

		return treeParser;
	}

	public String parseName(String fileName) {
		return (fileName.substring(9, fileName.length())).replaceAll(".java", "_java").replaceAll("/", ".");
	}
}
