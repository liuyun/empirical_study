/**
 * 
 */
package cn.edu.fudan.se.bean;

/**
 * @author echo
 *
 */
public class GitCommit {
	private String project;
	private String commitId;
	private String[] fileName;
	private String version;
	
	public GitCommit(String project, String commitId, String[] fileName, String version) {
		this.project = project;
		this.commitId = commitId;
		this.fileName = fileName;
		this.version = version;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getCommitId() {
		return commitId;
	}

	public void setCommitId(String commitId) {
		this.commitId = commitId;
	}

	public String[] getFileName() {
		return fileName;
	}

	public void setFileName(String[] fileName) {
		this.fileName = fileName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
