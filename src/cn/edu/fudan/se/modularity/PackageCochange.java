package cn.edu.fudan.se.modularity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cn.edu.fudan.se.constant.MySqlConstants;

public class PackageCochange {
	String project;
	Connection conn = null;
	Statement stmt = null;

	public static void main(String[] args) {
		PackageCochange pcc = new PackageCochange("cassandra");
		pcc.analyzeCochange(0);
	}

	public PackageCochange(String project) {
		this.project = project;
		try {
			Class.forName(MySqlConstants.DRIVER);
			conn = DriverManager.getConnection(MySqlConstants.URL, MySqlConstants.USER, MySqlConstants.PASSWORD);
			stmt = conn.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public HashMap<String, Integer>[] analyzeCochange(int threshold) {
		HashMap<String, Integer>[] maps = new HashMap[2];
		maps[0]=new HashMap<String, Integer>();
		maps[1]=new HashMap<String, Integer>();

		try {
			String sql = "select file_pair, count(file_pair) from file_pair_30file where project = 'cassandra' GROUP BY file_pair having count(file_pair) >= "
					+ threshold + " ORDER BY count(file_pair) desc";
			// System.out.println(sql);
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				String filePair = rs.getString(1);
				int count = Integer.parseInt(rs.getString(2));
				String[] packages = parsePackages(filePair);

				String p1 = packages[0];
				String p2 = packages[1];
				String packagePair = p1 + "-" + p2;
				if (p1.equals(p2)) {
					/*
					if (maps[0].containsKey(p1)) {
						maps[0].put(p1, maps[0].get(p1) + 2 * count);
					} else {
						maps[0].put(p1, count * 2);
					}

					if (maps[1].containsKey(packagePair)) {
						maps[1].put(packagePair, maps[1].get(packagePair) + 2 * count);
					} else {
						maps[1].put(packagePair, 2 * count);
					}**/
				} else {
					if (maps[0].containsKey(p1)) {
						maps[0].put(p1, maps[0].get(p1) + count);
					} else {
						maps[0].put(p1, count);
					}
					if (maps[0].containsKey(p2)) {
						maps[0].put(p2, maps[0].get(p2) + count);
					} else {
						maps[0].put(p2, count);
					}

					if (maps[1].containsKey(packagePair)) {
						maps[1].put(packagePair, maps[1].get(packagePair) + count);
					} else {
						maps[1].put(packagePair, count);
					}
				}
				// System.out.println(p1 + "\t" + maps[0].get(p1));
				// System.out.println(p2 + "\t" + maps[0].get(p2));
				// System.out.println(p1 + "-" + p2 + "\t" +
				// maps[1].get(p1 + "-" + p2));
			}

			Iterator<Map.Entry<String, Integer>> it = maps[0].entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) it.next();
				String packageName = entry.getKey();
				int count = entry.getValue();
				System.out.println(packageName + "\t" + count);
			}
			
			System.out.println("========================");
			
			it = maps[1].entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) it.next();
				String packagePair = entry.getKey();
				int count = entry.getValue();
				System.out.println(packagePair + "\t" + count);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return maps;
	}

	private String[] parsePackages(String filePair) {
		String[] fileNames = filePair.split("-");
		// System.out.println(fileNames[0]);
		String[] packages = new String[2];
		packages[0] = ((fileNames[0]).split("\\."))[3];
		packages[1] = ((fileNames[1]).split("\\."))[3];
		return packages;
	}
}
