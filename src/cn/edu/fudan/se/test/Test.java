package cn.edu.fudan.se.test;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext";
		String[] tokens = str.split("\n");
		System.out.println(str);
		for (String token : tokens) {
			System.out.println(token);
		}
	}

	public static int KMPMatch(char[] s, char[] p) {
		int sLen = s.length, pLen = p.length;
		int[] next = new int[pLen];
		getNextArray(p, next);

		int i = 0, j = 0;
		while (i < sLen && j < pLen) {
			if (j == -1 || s[i] == p[j]) {
				i++;
				j++;
			} else {
				j = next[j];
			}
		}
		if (j == pLen)
			return i - j;
		return -1;
	}

	public static void getNextArray(char[] p, int next[]) {
		int pLen = p.length;
		next[0] = -1;
		int j = 0;
		int k = -1;
		
		while (j < pLen - 1) {
			if (k == -1 || p[j] == p[k]) {
				j++;
				k++;
				if (p[j] != p[k])
					next[j] = k;
				else
					next[j] = next[k];
			} else {
				k = next[k];
			}
		}
	}
}
