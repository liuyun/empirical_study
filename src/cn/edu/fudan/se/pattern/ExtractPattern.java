package cn.edu.fudan.se.pattern;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import com.mysql.jdbc.PreparedStatement;

import cn.edu.fudan.se.constant.MySqlConstants;

public class ExtractPattern {
	private String project;
	private String directory;
	Connection conn = null;
	PreparedStatement stmt = null;

	public ExtractPattern(String project, String directory) {
		this.project = project;
		this.directory = directory;

		try {
			Class.forName(MySqlConstants.DRIVER);
			conn = DriverManager.getConnection(MySqlConstants.URL + project, MySqlConstants.USER,
					MySqlConstants.PASSWORD);
			stmt = (PreparedStatement) conn.prepareStatement("insert into pattern values (?, ?, ?, ?, ?, ?)");
			stmt.setString(1, project);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ExtractPattern ep = new ExtractPattern("cassandra", "C:/Users/echo/Desktop/pattern/cassandra");
		ep.extractRelation();
	}

	public void extractRelation() {
		ArrayList<File> files = getListFiles(new File(directory));
		for (File f : files) {
			parseFile(f);
		}
	}

	private void parseFile(File file) {
		String fileName1 = null;
		String fileName2 = null;
		String changeType1 = null;
		String changeType2 = null;
		String commitId = null;
		BufferedReader reader = null;
		HashSet<String> commits = new HashSet();
		try {
			// System.out.println("以行为单位读取文件内容，一次读一整行：");
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			// 一次读入一行，直到读入null为文件结束
			while ((tempString = reader.readLine()) != null) {
				int idx1 = tempString.indexOf("Aggregation:");
				if (idx1 != -1) {
					if (project.equals("cassandra")) {
						String[] tokens = tempString.substring(tempString.indexOf(":") + 1).split("-");
						fileName1 = tokens[0].replace("_java", ".java");
						fileName2 = tokens[1].replace("_java", ".java");
					} else {
						String[] tokens = tempString.substring(tempString.indexOf(":") + 1).split("--");
						fileName1 = tokens[0].replace("_java", ".java");
						fileName2 = tokens[1].replace("_java", ".java");
					}
				}
				int idx2 = tempString.indexOf("File:0");
				if (idx2 != -1)
					changeType1 = tempString.substring(tempString.indexOf("Type:") + 5);

				int idx3 = tempString.indexOf("File:1");
				if (idx3 != -1)
					changeType2 = tempString.substring(tempString.indexOf("Type:") + 5);

				if (!tempString.equals("") && Character.isDigit(tempString.charAt(0))) {
					commits.add(tempString.substring(tempString.indexOf(" ") + 1));
				}
			}

			System.out.println(fileName1 + "," + fileName2);
			// System.out.println(changeType1);
			// System.out.println(changeType2);
			for (String commit : commits) {
				// System.out.println(commit);
				stmt.setString(2, fileName1);
				stmt.setString(3, fileName2);
				stmt.setString(4, changeType1);
				stmt.setString(5, changeType2);
				stmt.setString(6, commit);
				stmt.executeUpdate();
			}
			// System.out.println();
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println(stmt.toString());
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
		}
	}

	private ArrayList<File> getListFiles(File directory) {
		ArrayList<File> files = new ArrayList<File>();
		if (directory.isFile()) {
			files.add(directory);
			return files;
		} else if (directory.isDirectory()) {
			File[] fileArr = directory.listFiles();
			for (int i = 0; i < fileArr.length; i++) {
				File fileOne = fileArr[i];
				files.addAll(getListFiles(fileOne));
			}
		}
		return files;
	}

}
