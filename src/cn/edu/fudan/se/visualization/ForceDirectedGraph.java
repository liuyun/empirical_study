package cn.edu.fudan.se.visualization;

import java.util.HashMap;

public class ForceDirectedGraph {
	HashMap<String, Integer> packageCount = new HashMap<String, Integer>();
	HashMap<String, Integer> packagePairCount = new HashMap<String, Integer>();
	
	public ForceDirectedGraph(HashMap<String, Integer> packageCount,HashMap<String, Integer> packagePairCount) {
		this.packageCount = packageCount;
		this.packagePairCount = packagePairCount;
	}
	
}
