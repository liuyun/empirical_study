package cn.edu.fudan.se.relation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

import cn.edu.fudan.se.constant.MySqlConstants;

public class RelationExtractor {
	private String project;
	private String directory;

	Connection conn = null;
	Statement stmt;

	public RelationExtractor(String project, String directory) {
		this.project = project;
		this.directory = directory;

		try {
			Class.forName(MySqlConstants.DRIVER);
			conn = DriverManager.getConnection(MySqlConstants.URL + project, MySqlConstants.USER, MySqlConstants.PASSWORD);
			stmt = conn.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void extractRelation() {
		ArrayList<File> files = getListFiles(new File(directory));
		for (File f : files) {
			// System.out.println(f.getAbsolutePath());
			parseFile(f);
			// System.exit(0);
		}
		// System.out.println(files.size());
	}

	private void parseFile(File f) {
		// TODO Auto-generated method stub
		String name = f.getName();
		if (name.endsWith(".dsm")) {
			String[] tokens = name.split("-");
			String fileName1 = tokens[1];
			String version = tokens[3].substring(0, tokens[3].length() - 4);
			// System.out.println(f.getAbsolutePath() + this.project + " " +
			// fileName1 + " " + version);
			ArrayList<String> fileNames = readFileByLines(f);
			for (String fileName2 : fileNames) {
				if (!fileName2.equals(fileName1)) {
					String sql = "insert into relation values('" + this.project + "','" + fileName1 + "','" + fileName2
							+ "','" + version + "','None')";
					// System.out.println(sql);
					try {
						stmt.execute(sql);
					} catch (Exception e) {
						System.out.println(sql);
						e.printStackTrace();
					}
				}
			}
		}
	}

	public ArrayList<File> getListFiles(File directory) {
		ArrayList<File> files = new ArrayList<File>();
		if (directory.isFile()) {
			files.add(directory);
			return files;
		} else if (directory.isDirectory()) {
			File[] fileArr = directory.listFiles();
			for (int i = 0; i < fileArr.length; i++) {
				File fileOne = fileArr[i];
				files.addAll(getListFiles(fileOne));
			}
		}
		return files;
	}

	/**
	 * 以行为单位读取文件，常用于读面向行的格式化文件
	 */
	public ArrayList<String> readFileByLines(File file) {
		ArrayList<String> fileNames = new ArrayList<String>();
		BufferedReader reader = null;
		try {
			// System.out.println("以行为单位读取文件内容，一次读一整行：");
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			int line = 1;
			// 一次读入一行，直到读入null为文件结束
			int lineOfRelation = Integer.MAX_VALUE;
			while ((tempString = reader.readLine()) != null) {
				if (line == 2) {
					lineOfRelation = Integer.parseInt(tempString) + 3;
				}
				if (line >= lineOfRelation) {
					// System.out.println("line " + line + ": " + tempString);
					fileNames.add(tempString);
				}
				line++;
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
		}
		return fileNames;
	}
}
