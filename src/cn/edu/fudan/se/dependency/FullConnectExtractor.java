package cn.edu.fudan.se.dependency;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import cn.edu.fudan.se.constant.MySqlConstants;

public class FullConnectExtractor {
	private String project;

	Connection conn = null;
	PreparedStatement pstmt = null;

	public FullConnectExtractor(String project) {
		this.project = project;

		try {
			Class.forName(MySqlConstants.DRIVER);
			conn = DriverManager.getConnection(MySqlConstants.URL, MySqlConstants.USER, MySqlConstants.PASSWORD);
			String sql = "select * from structure_relation where project = '" + project
					+ "' and file1_name = ? and file2_name = ? and version = ?";
			pstmt = conn.prepareStatement(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void extractCommit(String version) {
		String sql = "select * from commit where project = '" + project + "' and version = '" + version
				+ "' order by time desc";
		try {
			Statement stmt = conn.createStatement();
			ResultSet set = stmt.executeQuery(sql);
			while (set.next()) {
				String project = set.getString(1);
				String commitId = set.getString(2);
				// System.out.println(commitId);
				// System.out.print(project + "\t");
				// System.out.print(commitId + "\t");
				// System.out.println();
				String[] fileNames = extractFiles(project, commitId);
				if (isFullConnected(project, fileNames, version)) {
					// label this commit as SyntacticDependency
					addType(project, commitId, "Full-Connected");
				}
				// System.exit(0);
			}
		} catch (Exception e) {
			System.out.println(sql);
			e.printStackTrace();
		}
	}

	private void addType(String project, String commitId, String type) {
		String sql = "insert into co_change_type values('" + project + "','" + commitId + "','" + type + "')";
		// System.out.println(sql + "\n");
		// System.exit(0);
		try {
			Statement stmt = conn.createStatement();
			stmt.execute(sql);
		} catch (Exception e) {
			System.out.println(sql);
			e.printStackTrace();
		}

	}

	private boolean isFullConnected(String project, String[] fileName, String version) {
		int len = fileName.length;
		String[][] edges = new String[len][len];
		for (int i = 0; i < len; i++) {
			for (int j = 0; j < len; j++) {
				String relation = null;
				try {
					pstmt.setString(1, fileName[i]);
					pstmt.setString(2, fileName[j]);
					pstmt.setString(3, version);
					ResultSet set = pstmt.executeQuery();
					if (set.next()) {
						relation = set.getString(5);
					} else {
						relation = "None";
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				edges[i][j] = relation;
			}
		}
		// System.out.println();
		if (isConnectedGraph(edges)) {
			// System.out.println("\ntrue");
			return true;
		} else {
			// System.out.println("\nfalse");
			return false;
		}
	}

	private boolean isConnectedGraph(String[][] edges) {
		int n = edges.length;
		int[] visited = new int[n];
		dfs(visited, edges, 0);
		for (int v : visited) {
			if (v == 0)
				return false;
		}
		return true;
	}

	private void dfs(int[] visited, String[][] edges, int v) {
		// System.out.print(v + "-->");
		visited[v] = 1;
		int n = edges.length;
		for (int i = 0; i < n; i++) {
			if ((!edges[v][i].equals("None") || !edges[i][v].equals("None")) && visited[i] == 0) {
				// System.out.println(v + "-->" + i);
				dfs(visited, edges, i);
			}
		}
	}

	private String[] extractFiles(String project, String commitId) {
		String sql = "select * from file where project = '" + project + "' and commit_id = '" + commitId + "'";
		List<String> fileList = new ArrayList<String>();
		try {
			Statement stmt = conn.createStatement();
			ResultSet set = stmt.executeQuery(sql);
			while (set.next()) {
				fileList.add(set.getString(3));
			}
		} catch (Exception e) {
			System.out.println(sql);
			e.printStackTrace();
		}
		String[] fileName = new String[fileList.size()];
		for (int i = 0; i < fileName.length; i++) {
			fileName[i] = fileList.get(i);
			// System.out.println(fileList.get(i));
		}
		return fileName;
	}
}
