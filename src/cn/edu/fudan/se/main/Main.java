package cn.edu.fudan.se.main;

public class Main {

	public static void main(String[] args) {
		// Analyze cassandra
		// Launch.extractCassandraLog();
		// Launch.countCassandraFilePair();
		// Launch.countCassandraFileTriple();
		// Launch.countCassandraFileQuadruple();
		// Analyze cfx
		// Launch.extractCxfLog();
		// Launch.countCxfFilePair();
		// Launch.countCxfFileTriple();
		// Launch.countCxfFileQuadruple();

		// Launch.extractHadoopLog();
		// Launch.countHadoopFilePair();
		// Launch.countHadoopFileTriple();
		Launch.countHadoopFileQuadruple();

		// Launch.extractHBaseLog();
		// Launch.countHBaseFilePair();
		// Launch.countHBaseFileTriple();
		// Launch.countHBaseFileQuadruple();

		// Launch.extractPDFBoxLog();
		// Launch.countPDFBoxFilePair();
		// Launch.countPDFBoxFileTriple();
		// Launch.countPDFBoxFileQuadruple();

		// Launch.extractWicketLog();
		// Launch.countWicketFilePair();
		// Launch.countWicketFileTriple();
		// Launch.countWicketFileQuadruple();

		// Launch.extractCamelLog();
		// Launch.countCamelFilePair();
		// Launch.countCamelFileTriple();
		// Launch.countCamelFileQuadruple();
		System.out.println("Main completed!");
	}
}
