package cn.edu.fudan.se.main;

import java.io.IOException;
import java.sql.Timestamp;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.errors.AmbiguousObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.errors.RevisionSyntaxException;

import cn.edu.fudan.se.analysis.RankFilePair;
import cn.edu.fudan.se.analysis.RankFileQuadruple;
import cn.edu.fudan.se.analysis.RankFileTriple;
import cn.edu.fudan.se.archdebt.ArchDebtExtractor;
import cn.edu.fudan.se.archdebt.ArchDebtHistoryExtractor;
import cn.edu.fudan.se.archdebt.ArchDebtRankExtractor;
import cn.edu.fudan.se.constant.RepoConstants;
import cn.edu.fudan.se.dependency.FullConnectExtractor;
import cn.edu.fudan.se.git.GitExtractor;
import cn.edu.fudan.se.git.GitLogExtractor;
import cn.edu.fudan.se.relation.RelationParser;

public class Launch {
	/**
	 * Extract commit history of cassandra. Specify git repository path and
	 * project name. Use time to accurately find the tag.
	 */
	public static void extractCassandra() {
		try {
			GitExtractor gitExtractor = new GitExtractor(RepoConstants.CASSANDRA__PATH, RepoConstants.CASSANDRA_NAME);
			/**
			 * Timestamp begin = Timestamp.valueOf("2009-03-02 15:57:22");
			 * Timestamp end = Timestamp.valueOf("2010-03-14 07:17:20");
			 * gitExtractor.track(begin, end, "0.0.0", "0.5.0");
			 * 
			 * begin = Timestamp.valueOf("2010-03-14 07:17:20"); end =
			 * Timestamp.valueOf("2010-08-28 03:51:19");
			 * gitExtractor.track(begin, end, "0.5.0", "0.6.5");
			 * 
			 * begin = Timestamp.valueOf("2010-08-28 03:51:19"); end =
			 * Timestamp.valueOf("2011-02-15 03:33:44");
			 * gitExtractor.track(begin, end, "0.6.5", "0.7.1");
			 * 
			 * begin = Timestamp.valueOf("2011-02-15 03:33:44"); end =
			 * Timestamp.valueOf("2011-11-07 23:03:24");
			 * gitExtractor.track(begin, end, "0.7.1", "1.0.2");
			 * 
			 * begin = Timestamp.valueOf("2011-11-07 23:03:24"); end =
			 * Timestamp.valueOf("2012-05-04 23:53:39");
			 * gitExtractor.track(begin, end, "1.0.2", "1.0.10");
			 * 
			 * begin = Timestamp.valueOf("2012-05-04 23:53:39"); end =
			 * Timestamp.valueOf("2012-11-27 16:34:48");
			 * gitExtractor.track(begin, end, "1.0.10", "1.1.7");
			 * 
			 * begin = Timestamp.valueOf("2012-11-27 16:34:48"); end =
			 * Timestamp.valueOf("2013-05-15 21:21:14");
			 * gitExtractor.track(begin, end, "1.1.7", "1.2.5");
			 * 
			 * begin = Timestamp.valueOf("2013-05-15 21:21:14"); end =
			 * Timestamp.valueOf("2013-11-22 15:45:22");
			 * gitExtractor.track(begin, end, "1.2.5", "2.0.3");
			 * 
			 * begin = Timestamp.valueOf("2013-11-22 15:45:22"); end =
			 * Timestamp.valueOf("2014-05-21 17:48:21");
			 * gitExtractor.track(begin, end, "2.0.3", "2.0.8");
			 * 
			 * begin = Timestamp.valueOf("2014-05-21 17:48:21"); end =
			 * Timestamp.valueOf("2014-11-06 07:49:59");
			 * gitExtractor.track(begin, end, "2.0.8", "2.1.2");
			 */
			// recent history
			Timestamp begin = Timestamp.valueOf("2014-11-06 07:49:59");
			Timestamp end = Timestamp.valueOf("2015-04-27 22:25:52");
			gitExtractor.track(begin, end, "2.1.2", "2.1.5");

			begin = Timestamp.valueOf("2015-04-27 22:25:52");
			end = Timestamp.valueOf("2015-10-14 00:18:55");
			gitExtractor.track(begin, end, "2.1.5", "2.1.11");

			begin = Timestamp.valueOf("2015-10-14 00:18:55");
			end = Timestamp.valueOf("2016-04-19 05:44:07");
			gitExtractor.track(begin, end, "2.1.11", "2.1.14");

		} catch (RevisionSyntaxException e) {
			e.printStackTrace();
		} catch (NoHeadException e) {
			e.printStackTrace();
		} catch (MissingObjectException e) {
			e.printStackTrace();
		} catch (IncorrectObjectTypeException e) {
			e.printStackTrace();
		} catch (AmbiguousObjectException e) {
			e.printStackTrace();
		} catch (GitAPIException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Launch.extractCassandra complete!");
	}

	public static void extractCassandraRelation() {
		RelationParser rp = new RelationParser(RepoConstants.CASSANDRA_NAME, RepoConstants.CASSANDRA_RELATION_PATH);
		rp.extractRelation();
		System.out.println("Launch.extractCassandraRelation complete!");
	}

	public static void extractCassandraConnection() {
		FullConnectExtractor fcExtractor = new FullConnectExtractor(RepoConstants.CASSANDRA_NAME);
		fcExtractor.extractCommit("2.1.14");
		System.out.println("2.1.14 complete!");
		fcExtractor.extractCommit("2.1.11");
		System.out.println("2.1.11 complete!");
		fcExtractor.extractCommit("2.1.5");
		System.out.println("2.1.5 complete!");
		fcExtractor.extractCommit("2.1.2");
		System.out.println("2.1.2 complete!");
		fcExtractor.extractCommit("2.0.8");
		System.out.println("2.0.8 complete!");
		fcExtractor.extractCommit("2.0.3");
		System.out.println("2.0.3 complete!");
		fcExtractor.extractCommit("1.2.5");
		System.out.println("1.2.5 complete!");
		fcExtractor.extractCommit("1.1.7");
		System.out.println("1.1.7 complete!");
		fcExtractor.extractCommit("1.0.10");
		System.out.println("1.0.10 complete!");
		fcExtractor.extractCommit("1.0.2");
		System.out.println("1.0.2 complete!");
		fcExtractor.extractCommit("0.7.1");
		System.out.println("0.7.1 complete!");
		fcExtractor.extractCommit("0.6.5");
		System.out.println("0.6.5 complete!");
		fcExtractor.extractCommit("0.5.0");
		System.out.println("0.5.0 complete!");

		System.out.println("Launch.extractCassandraConnection complete!");

	}

	public static void countFilePair(String project, String table) {
		RankFilePair rfp = new RankFilePair(project);
		rfp.extractFilePair(table);
	}

	public static void countFileTriple(String project, String table, int threshold) {
		RankFileTriple rft = new RankFileTriple(project);
		rft.extractFileTriple(table, threshold);
		System.out.println("Launch.countFileTriple complete!");
	}

	public static void countCassandraFilePair() {
		countFilePair(RepoConstants.CASSANDRA_NAME, "file_30file");
		System.out.println("Launch.countCassandraFilePair complete!");
	}

	public static void countCassandraFileTriple() {
		countFileTriple(RepoConstants.CASSANDRA_NAME, "file_pair_30file", 10);
		System.out.println("Launch.countCassandraFileTriple complete!");
	}

	public static void countCassandraFileQuadruple() {
		countFileQuadruple(RepoConstants.CASSANDRA_NAME, "file_triple_10count_30file", 10);
		System.out.println("Launch.countCassandraFileQuadruple complete!");
	}

	public static void countFileQuadruple(String project, String table, int threshold) {
		RankFileQuadruple rft = new RankFileQuadruple(project);
		rft.extractFileQuadruple(table, threshold);
		System.out.println("Launch.countFileQuadruple complete!");
	}

	public static void extractCassandraArchDebt() {
		ArchDebtExtractor ade = new ArchDebtExtractor(RepoConstants.CASSANDRA_NAME);
		ade.extractArchDebt("AD");
		ade.extractArchDebt("AS");
		ade.extractArchDebt("HB");
		ade.extractArchDebt("MV");
		ade.extractMemberFiles("AD");
		ade.extractMemberFiles("AS");
		ade.extractMemberFiles("HB");
		ade.extractMemberFiles("MV");
		System.out.println("Launch.extractArchDebt complete!");
	}

	public static void extractCassandraArchDebtHistory() {
		ArchDebtHistoryExtractor adhe = new ArchDebtHistoryExtractor(RepoConstants.CASSANDRA_NAME);
		// adhe.extractArchDebtHistory("2.1.2", "HB");
		// adhe.extractArchDebtHistory("2.1.2", "AS");
		// adhe.extractArchDebtHistory("2.1.2", "AD");
		adhe.extractArchDebtHistory("2.1.2", "MV");
		System.out.println("Launch.extractCassandraArchDebtHistory complete!");
	}

	public static void extractCassandraArchDebtRank() {
		ArchDebtRankExtractor adre = new ArchDebtRankExtractor(RepoConstants.CASSANDRA_NAME);
		adre.extractArchDebt("AD");
		adre.extractArchDebt("AS");
		adre.extractArchDebt("HB");
		adre.extractArchDebt("MV");
		System.out.println("Launch.extractCassandraArchDebtRank complete!");
	}

	public static void extractCassandraLog() {
		GitLogExtractor gle = new GitLogExtractor(RepoConstants.CASSANDRA_NAME);
		String gitHome = "D:\\Tools\\Git\\bin\\";
		String cmd = "git log --name-status";
		String dir = "D:\\echo\\lab\\research\\co-change\\projects\\cassandra";
		gle.gitCommand(gitHome, cmd, dir);
		// gle.parseGitLog("C:\\Users\\echo\\Desktop\\cassandra.log");
	}

	public static void extractCxfLog() {
		GitLogExtractor gle = new GitLogExtractor(RepoConstants.CXF_NAME);
		String gitHome = "D:\\Tools\\Git\\bin\\";
		String cmd = "git log --name-status";
		String dir = "D:\\echo\\lab\\research\\co-change\\projects\\cxf";
		gle.gitCommand(gitHome, cmd, dir);
		// System.out.println(gle.project);
		// gle.parseGitLog("C:\\Users\\echo\\Desktop\\cxf.log");
	}

	public static void countCxfFilePair() {
		countFilePair(RepoConstants.CXF_NAME, "file_30file");
		System.out.println("Launch.countCxfFilePair complete!");
	}

	public static void countCxfFileTriple() {
		countFileTriple(RepoConstants.CXF_NAME, "file_pair_30file", 5);
		System.out.println("Launch.countCxfFileTriple complete!");
	}

	public static void countCxfFileQuadruple() {
		countFileQuadruple(RepoConstants.CXF_NAME, "file_triple_5count_30file", 5);
		System.out.println("Launch.countCxfFileQuadruple complete!");
	}

	public static void extractHadoopLog() {
		GitLogExtractor gle = new GitLogExtractor(RepoConstants.HADOOP_NAME);
		String gitHome = "D:\\Tools\\Git\\bin\\";
		String cmd = "git log --name-status";
		String dir = "D:\\echo\\lab\\research\\co-change\\projects\\hadoop";
		gle.gitCommand(gitHome, cmd, dir);
		gle.parseGitLog("C:\\Users\\echo\\Desktop\\hadoop.log");

	}

	public static void extractPdfboxLog() {
		GitLogExtractor gle = new GitLogExtractor(RepoConstants.PDFBOX_NAME);
		String gitHome = "D:\\Tools\\Git\\bin\\";
		String cmd = "git log --name-status";
		String dir = "D:\\echo\\lab\\research\\co-change\\projects\\pdfbox";
		gle.gitCommand(gitHome, cmd, dir);

	}

	public static void countHadoopFilePair() {
		countFilePair(RepoConstants.HADOOP_NAME, "file_30file");
		System.out.println("Launch.countHadoopFilePair complete!");
	}

	public static void countHadoopFileTriple() {
		countFileTriple(RepoConstants.HADOOP_NAME, "file_pair_30file", 5);
		System.out.println("Launch.countHadoopFileTriple complete!");
	}

	public static void countHadoopFileQuadruple() {
		countFileQuadruple(RepoConstants.HADOOP_NAME, "file_triple_5count_30file", 5);
		System.out.println("Launch.countHadoopFileQuadruple complete!");

	}

	public static void extractHBaseLog() {
		GitLogExtractor gle = new GitLogExtractor(RepoConstants.HBASE_NAME);
		String gitHome = "D:\\Tools\\Git\\bin\\";
		String cmd = "git log --name-status";
		String dir = "D:\\echo\\lab\\research\\co-change\\projects\\hbase";
		gle.gitCommand(gitHome, cmd, dir);
		gle.parseGitLog("C:\\Users\\echo\\Desktop\\HBase.log");

	}

	public static void countHBaseFilePair() {
		countFilePair(RepoConstants.HBASE_NAME, "file_30file");
		System.out.println("Launch.countHBaseFilePair complete!");
	}

	public static void countHBaseFileTriple() {
		countFileTriple(RepoConstants.HBASE_NAME, "file_pair_30file", 5);
		System.out.println("Launch.countHbaseFileTriple complete!");
	}

	public static void countHBaseFileQuadruple() {
		countFileQuadruple(RepoConstants.HBASE_NAME, "file_triple_5count_30file", 5);
		System.out.println("Launch.countHbaseFileQuadruple complete!");
	}

	public static void extractPDFBoxLog() {
		GitLogExtractor gle = new GitLogExtractor(RepoConstants.PDFBOX_NAME);
		String gitHome = "D:\\Tools\\Git\\bin\\";
		String cmd = "git log --name-status";
		String dir = "D:\\echo\\lab\\research\\co-change\\projects\\pdfbox";
		gle.gitCommand(gitHome, cmd, dir);
		gle.parseGitLog("C:\\Users\\echo\\Desktop\\" + RepoConstants.PDFBOX_NAME + ".log");
	}

	public static void countPDFBoxFilePair() {
		countFilePair(RepoConstants.PDFBOX_NAME, "file_30file");
		System.out.println("Launch.countPDFBoxFilePair complete!");
	}

	public static void countPDFBoxFileTriple() {
		countFileTriple(RepoConstants.PDFBOX_NAME, "file_pair_30file", 0);
		System.out.println("Launch.countPDFBoxFileTriple complete!");
	}

	public static void countPDFBoxFileQuadruple() {
		countFileQuadruple(RepoConstants.PDFBOX_NAME, "file_triple_30file", 0);
		System.out.println("Launch.countPDFBoxFileQuadruple complete!");
	}

	public static void extractWicketLog() {
		GitLogExtractor gle = new GitLogExtractor(RepoConstants.WICKET_NAME);
		// String gitHome = "D:\\Tools\\Git\\bin\\";
		// String cmd = "git log --name-status";
		// String dir = "D:\\echo\\lab\\research\\co-change\\projects\\wicket";
		// gle.gitCommand(gitHome, cmd, dir);
		gle.parseGitLog("C:\\Users\\echo\\Desktop\\" + RepoConstants.WICKET_NAME + ".log");

	}

	public static void countWicketFilePair() {
		countFilePair(RepoConstants.WICKET_NAME, "file_30file");
		System.out.println("Launch.countPDFBoxFilePair complete!");
	}

	public static void countWicketFileTriple() {
		countFileTriple(RepoConstants.WICKET_NAME, "file_pair_30file", 5);
		System.out.println("Launch.countWicketFileTriple complete!");
	}

	public static void countWicketFileQuadruple() {
		countFileQuadruple(RepoConstants.WICKET_NAME, "file_triple_30file", 5);
		System.out.println("Launch.countWicketFileQuadruple complete!");
	}

	public static void extractCamelLog() {
		GitLogExtractor gle = new GitLogExtractor(RepoConstants.CAMEL_NAME);
		String gitHome = "D:\\Tools\\Git\\bin\\";
		String cmd = "git log --name-status";
		String dir = "D:\\echo\\lab\\research\\co-change\\projects\\" + RepoConstants.CAMEL_NAME;
		gle.gitCommand(gitHome, cmd, dir);
		gle.parseGitLog("C:\\Users\\echo\\Desktop\\" + RepoConstants.CAMEL_NAME + ".log");
	}

	public static void countCamelFilePair() {
		countFilePair(RepoConstants.CAMEL_NAME, "file_30file");
		System.out.println("Launch.countCamelFilePair complete!");
	}

	public static void countCamelFileTriple() {
		countFileTriple(RepoConstants.CAMEL_NAME, "file_pair_30file", 5);
		System.out.println("Launch.countCamelFileTriple complete!");	
	}

	public static void countCamelFileQuadruple() {
		countFileQuadruple(RepoConstants.CAMEL_NAME, "file_triple_5count_30file", 5);
		System.out.println("Launch.countCamelFileQuadruple complete!");
	}
}
