package cn.edu.fudan.se.archdebt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.csvreader.CsvReader;

import cn.edu.fudan.se.constant.ArchDebtConstants;
import cn.edu.fudan.se.constant.MySqlConstants;

public class ArchDebtExtractor {
	private String project = null;
	private Connection conn = null;
	private PreparedStatement pstmt = null;

	public ArchDebtExtractor(String project) {
		this.project = project;
		try {
			Class.forName(MySqlConstants.DRIVER);
			conn = DriverManager.getConnection(MySqlConstants.URL, MySqlConstants.USER, MySqlConstants.PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void extractArchDebt(String debtType) {
		String sql = "insert into arch_debt (project, version, type, anchor, size, churn) values(?,?,?,?,?,?)";
		try {
			pstmt = conn.prepareStatement(sql);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String path = ArchDebtConstants.DIRECTORY + this.project + "/" + debtType + "/";
		String[] versions = { "0.5.0", "0.6.5", "0.7.1", "1.0.2", "1.0.10", "1.1.7", "1.2.5", "2.0.3", "2.0.8",
				"2.1.2" };

		for (String version : versions) {
			String csvName = path + "All-" + debtType + "-Patterns-Index-cassandra-" + version + ".csv";
			// System.out.println(csvName);

			try {
				CsvReader r = new CsvReader(csvName, ',', Charset.forName("UTF-8"));
				// 读取表头
				r.readHeaders();
				// 逐条读取记录，直至读完
				while (r.readRecord()) {
					// 按列名读取这条记录的值
					String anchor = r.get("anchor");
					int size = Integer.parseInt(r.get("size"));
					int churn = Integer.parseInt(r.get("churn"));
					pstmt.setString(1, this.project);
					pstmt.setString(2, version);
					pstmt.setString(3, debtType);
					pstmt.setString(4, anchor);
					pstmt.setInt(5, size);
					pstmt.setInt(6, churn);
					pstmt.executeUpdate();
				}
				r.close();
			} catch (IOException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void extractMemberFiles(String debtType) {
		String path = ArchDebtConstants.DIRECTORY + this.project + "/" + debtType + "/";
		String sql = "select * from arch_debt where project = ? and type = ?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, this.project);
			pstmt.setString(2, debtType);
			ResultSet rs = pstmt.executeQuery();

			String insertSql = "insert into arch_debt_group (project, version, type, anchor, member, size, churn) values(?,?,?,?,?,?,?)";
			PreparedStatement insertPstmt = conn.prepareStatement(insertSql);
			while (rs.next()) {
				String version = rs.getString(2);
				String anchor = rs.getString(4);
				int size = rs.getInt(5);
				int churn = rs.getInt(6);
				String fileName = path + anchor + "/" + debtType + "-" + anchor + "-" + project + "-" + version
						+ ".dsm";
				// System.out.println(fileName);
				File f = new File(fileName);
				List<String> fileNames = readFileByLines(f);
				for (String member : fileNames) {
					if (!member.equals(anchor)) {
						insertPstmt.setString(1, this.project);
						insertPstmt.setString(2, version);
						insertPstmt.setString(3, debtType);
						insertPstmt.setString(4, anchor);
						insertPstmt.setString(5, member);
						insertPstmt.setInt(6, size);
						insertPstmt.setInt(7, churn);
						insertPstmt.executeUpdate();
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<String> readFileByLines(File file) {
		ArrayList<String> fileNames = new ArrayList<String>();
		BufferedReader reader = null;
		try {
			// System.out.println("以行为单位读取文件内容，一次读一整行：");
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			int line = 1;
			// 一次读入一行，直到读入null为文件结束
			int lineOfRelation = Integer.MAX_VALUE;
			while ((tempString = reader.readLine()) != null) {
				if (line == 2) {
					lineOfRelation = Integer.parseInt(tempString) + 3;
				}
				if (line >= lineOfRelation) {
					// System.out.println("line " + line + ": " + tempString);
					fileNames.add(tempString);
				}
				line++;
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
		}
		return fileNames;
	}
}
