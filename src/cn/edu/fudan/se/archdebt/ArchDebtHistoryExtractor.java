package cn.edu.fudan.se.archdebt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import cn.edu.fudan.se.constant.MySqlConstants;

public class ArchDebtHistoryExtractor {
	private String project = null;
	private Connection conn = null;
	private PreparedStatement pstmt = null;

	public ArchDebtHistoryExtractor(String project) {
		this.project = project;
		try {
			Class.forName(MySqlConstants.DRIVER);
			conn = DriverManager.getConnection(MySqlConstants.URL, MySqlConstants.USER, MySqlConstants.PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void extractArchDebtHistory(String version, String type) {
		String sql = "select * from arch_debt_group where project = '" + this.project + "' and type = '" + type
				+ "' and revision = '" + version + "'";
		String sql2 = "select * from rank_file_pair where project = ? and file_pair = ?";
		// System.out.println(sql);
		try {
			pstmt = conn.prepareStatement(sql2);
			pstmt.setString(1, this.project);

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			String preAnchor = null;
			List<String> members = new ArrayList<String>();
			String anchor = null;
			String member = null;

			while (rs.next()) {
				anchor = rs.getString(4);
				member = rs.getString(5);

				if (preAnchor == null)
					preAnchor = anchor;
				if (preAnchor.equals(anchor)) {
					members.add(member);
				} else {
					queryHistory(version, type, preAnchor, members);
					preAnchor = anchor;
					members.clear();
					members.add(member);
				}
			}
			queryHistory(version, type, preAnchor, members);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void queryHistory(String version, String type, String anchor, List<String> members) {
		// System.out.println(anchor);
		// System.out.println(members);
		int size = members.size();
		int count = 0;
		try {
			for (String member : members) {
				String filePair = null;
				if (anchor.compareTo(member) < 0) {
					filePair = anchor + "-" + member;
				} else {
					filePair = member + "-" + anchor;
				}
				pstmt.setString(2, filePair);
				// System.out.println(filePair);
				ResultSet rs = pstmt.executeQuery();

				while (rs.next()) {
					count += rs.getInt(3);
					// System.out.println(rs.getString(2));
				}
			}
			String sql = "update arch_debt set avg_count = " + (double) count / (double) size + " where project = '"
					+ project + "' and revision = '" + version + "' and type = '" + type + "' and anchor = '" + anchor
					+ "'";
//			System.out.println(sql);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
