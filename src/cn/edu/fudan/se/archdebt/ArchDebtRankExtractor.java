package cn.edu.fudan.se.archdebt;

import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.csvreader.CsvReader;

import cn.edu.fudan.se.constant.ArchDebtConstants;
import cn.edu.fudan.se.constant.MySqlConstants;

public class ArchDebtRankExtractor {
	private String project = null;
	private Connection conn = null;
	private PreparedStatement pstmt = null;

	public ArchDebtRankExtractor(String project) {
		this.project = project;
		try {
			Class.forName(MySqlConstants.DRIVER);
			conn = DriverManager.getConnection(MySqlConstants.URL, MySqlConstants.USER, MySqlConstants.PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void extractArchDebt(String debtType) {
		String sql = "insert into rank_arch_debt (project, version, type, anchor, size, churn, rank, cost_per, size_per) values(?,?,?,?,?,?,?,?,?)";
		try {
			pstmt = conn.prepareStatement(sql);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String path = ArchDebtConstants.DIRECTORY + this.project + "/" + debtType + "/";
		String[] versions = { "0.5.0", "0.6.5", "0.7.1", "1.0.2", "1.0.10", "1.1.7", "1.2.5", "2.0.3", "2.0.8",
				"2.1.2" };

		for (String version : versions) {
			String csvName = path + "Ranked-" + debtType + "-Index-" + this.project + "-" + version + ".csv";
			// System.out.println(csvName);

			try {
				CsvReader r = new CsvReader(csvName, ',', Charset.forName("UTF-8"));
				// 读取表头
				r.readHeaders();
				r.readRecord();
				// 逐条读取记录，直至读完
				// int i = 1;
				while (r.readRecord()) {
					// 按列名读取这条记录的值
					int order = Integer.parseInt(r.get(0));
					String anchor = r.get(1);
					int size = Integer.parseInt(r.get(2));
					int churn = (int) Double.parseDouble(r.get(5));
					double cost_per = Double.parseDouble(r.get(3));
					double size_per = Double.parseDouble(r.get(4));
					// System.out.println(project + "\t" + debtType + "\t" +
					// order + "\t" + anchor + "\t" + size + "\t"
					// + churn + "\t" + cost_per + "\t" + size_per);
					pstmt.setString(1, this.project);
					pstmt.setString(2, version);
					pstmt.setString(3, debtType);
					pstmt.setString(4, anchor);
					pstmt.setInt(5, size);
					pstmt.setInt(6, churn);
					pstmt.setInt(7, order);
					pstmt.setDouble(8, cost_per);
					pstmt.setDouble(9, size_per);
					pstmt.executeUpdate();
				}
				r.close();
			} catch (IOException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
