package cn.edu.fudan.se.constant;

public class RepoConstants {
	public static final String CASSANDRA__PATH = "D:/echo/lab/research/co-change/projects/cassandra/.git";
	public static final String CASSANDRA_NAME = "cassandra";
	public static final String CASSANDRA_RELATION_PATH = "D:/echo/lab/research/co-change/understand/cassandra";

	public static final String CXF__PATH = "D:/echo/lab/research/co-change/projects/cfx/.git";
	public static final String CXF_NAME = "CXF";

	public static final String HADOOP__PATH = "D:/echo/lab/research/co-change/projects/hadoop/.git";
	public static final String HADOOP_NAME = "Hadoop";

	public static final String HBASE__PATH = "D:/echo/lab/research/co-change/projects/hbase/.git";
	public static final String HBASE_NAME = "HBase";

	public static final String PDFBOX__PATH = "D:/echo/lab/research/co-change/projects/pdfbox/.git";
	public static final String PDFBOX_NAME = "PDFBox";

	public static final String WICKET__PATH = "D:/echo/lab/research/co-change/projects/wicket/.git";
	public static final String WICKET_NAME = "Wicket";
	
	public static final String CAMEL__PATH = "D:/echo/lab/research/co-change/projects/camel/.git";
	public static final String CAMEL_NAME = "Camel";
}
