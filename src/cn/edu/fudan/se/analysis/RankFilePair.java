package cn.edu.fudan.se.analysis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.edu.fudan.se.constant.MySqlConstants;

public class RankFilePair {
	private String project;
	Connection conn = null;

	public RankFilePair(String project) {
		this.project = project;

		try {
			Class.forName(MySqlConstants.DRIVER);
			conn = DriverManager.getConnection(MySqlConstants.URL, MySqlConstants.USER, MySqlConstants.PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void extractFilePair(String tableName) {
		String sql = "select * from " + tableName + " where project = '" + this.project + "' order by time desc";
//		 System.out.println(sql);
		Statement stmt;
		String preCommitId = "initial";
		Timestamp preTime = null;
		List<String> fileNames = new ArrayList<String>();
		try {
			stmt = conn.createStatement();
			ResultSet set = stmt.executeQuery(sql);
			while (set.next()) {
				String commitId = set.getString(2);
				String fileName = set.getString(3);
				Timestamp time = set.getTimestamp(5);

				if (preCommitId.equals("initial")) {
					preCommitId = commitId;
					preTime = time;
				}

				if (commitId.equals(preCommitId)) {
					fileNames.add(fileName);
				} else {
					// System.out.println("add");
					insertFilePair(fileNames, preCommitId, preTime);
					preCommitId = commitId;
					preTime = time;
					fileNames.clear();
					fileNames.add(fileName);
				}
				// System.out.println(project + "\t" + commitId + "\t" +
				// fileName);
			}
			if (fileNames.size() >= 2)
				insertFilePair(fileNames, preCommitId, preTime);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("RankFilePair.extractFilePair complete");
		return;
	}

	private void insertFilePair(List<String> fileNames, String commitId, Timestamp time) {
		if (fileNames.size() < 2)
			return;
		Collections.sort(fileNames);
		for (int i = 0; i < fileNames.size(); i++) {
			for (int j = i + 1; j < fileNames.size(); j++) {
				String filePair = fileNames.get(i) + "--" + fileNames.get(j);
				String sql = "insert into file_pair_30file values ('" + project + "','" + commitId + "','" + filePair
						+ "','" + time + "')";
				// System.out.println(sql);

				try {
					Statement stmt = conn.createStatement();
					stmt.execute(sql);
				} catch (SQLException e) {
					System.out.println(sql);
					e.printStackTrace();
				}
			}
		}
	}
}
