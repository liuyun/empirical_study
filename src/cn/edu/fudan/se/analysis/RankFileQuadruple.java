package cn.edu.fudan.se.analysis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import cn.edu.fudan.se.constant.MySqlConstants;

public class RankFileQuadruple {
	private String project;
	Connection conn = null;

	public RankFileQuadruple(String project) {
		this.project = project;

		try {
			Class.forName(MySqlConstants.DRIVER);
			conn = DriverManager.getConnection(MySqlConstants.URL, MySqlConstants.USER, MySqlConstants.PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void extractFileQuadruple(String tableName, int threshold) {
		String sql = "select * from " + tableName
				+ " where file_triple in (select file_triple from file_triple_5count_30file_rank where project = '"
				+ project + "' and count >= " + threshold + ") ORDER BY time DESC";

		Statement stmt;
		String preCommitId = "initial";
		Timestamp preTime = null;
		HashSet<String> fileNames = new HashSet<String>();
		try {
			stmt = conn.createStatement();
			ResultSet set = stmt.executeQuery(sql);
			while (set.next()) {
				// String project = set.getString(1);
				String commitId = set.getString(2);
				String filePair = set.getString(3);
				Timestamp time = set.getTimestamp(4);

				if (preCommitId.equals("initial")) {
					preCommitId = commitId;
					preTime = time;
				}

				if (commitId.equals(preCommitId)) {
					addFileTriple(fileNames, filePair);
				} else {
					// System.out.println("add");
					insertFileQuadruple(fileNames, preCommitId, preTime);
					preCommitId = commitId;
					preTime = time;
					fileNames.clear();
					addFileTriple(fileNames, filePair);
				}
				// System.out.println(project + "\t" + commitId + "\t"
				// +filePair);
			}
			if (fileNames.size() >= 4)
				insertFileQuadruple(fileNames, preCommitId, preTime);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("RankFileTriple.extractFileQuadruple complete");
		return;
	}

	private void insertFileQuadruple(HashSet<String> files, String commitId, Timestamp time) {
		if (files.size() < 4)
			return;
		List<String> fileNames = new ArrayList<String>(files);
		Collections.sort(fileNames);

		for (int i = 0; i < fileNames.size() - 2; i++) {
			for (int j = i + 1; j < fileNames.size() - 1; j++) {
				for (int k = j + 1; k < fileNames.size(); k++) {
					for (int l = k + 1; l < fileNames.size(); l++) {
						String fileQuadruple = fileNames.get(i) + "--" + fileNames.get(j) + "--" + fileNames.get(k) + "--"
								+ fileNames.get(l);
						// Note the table name in sql should change where the
						// threshold is different
						String sql = "insert into file_quadruple_5count_30file values ('" + project + "','" + commitId
								+ "','" + fileQuadruple + "','" + time + "')";
						// System.out.println(project + "\t" + commitId + "\t" +
						// fileTriple + "\t" + time);

						try {
							Statement stmt = conn.createStatement();
							stmt.execute(sql);
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	private void addFileTriple(HashSet<String> fileNames, String fileTriple) {
		String[] files = fileTriple.split("--");
		fileNames.add(files[0]);
		fileNames.add(files[1]);
		fileNames.add(files[2]);
	}
}
