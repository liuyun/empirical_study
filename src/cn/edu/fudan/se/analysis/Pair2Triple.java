package cn.edu.fudan.se.analysis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import cn.edu.fudan.se.constant.MySqlConstants;

public class Pair2Triple {
	String project = null;
	Connection conn = null;
	Statement stmt = null;

	public Pair2Triple(String project) {
		this.project = project;

		try {
			Class.forName(MySqlConstants.DRIVER);
			conn = DriverManager.getConnection(MySqlConstants.URL, MySqlConstants.USER, MySqlConstants.PASSWORD);
			stmt = conn.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getFilePairAmongTriple(int threshold1, int threshold2) {
		String sql = "select * from rank_file_pair where project = '" + project + "' limit 0," + threshold1;
		List<String> filePairList = new ArrayList<String>();

		String sql2 = "select * from rank_file_triple where project = '" + project + "' limit 0," + threshold2;
		List<String> fileTripleList = new ArrayList<String>();
		try {
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				filePairList.add(rs.getString(2));
			}

			rs = stmt.executeQuery(sql2);
			while (rs.next()) {
				fileTripleList.add(rs.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int i = 1;
		int miss = 0;
		boolean flag = false;
		for (i = 0; i < filePairList.size(); i++) {
			flag = false;
			String filePair = filePairList.get(i);
			System.out.println(filePair);
			filePair = ".*".concat(filePair.replaceAll("-", ".*")).concat(".*");

			for (int j = 0; j < fileTripleList.size(); j++) {
				if (fileTripleList.get(j).matches(filePair)) {
					System.out.println((j + 1) + "  " + fileTripleList.get(j));
					flag = true;
					break;
				}
			}
			if (!flag)
				miss++;
		}
		System.out.println((int) ((double) miss / filePairList.size() * 1000) / 1000.0);
	}

	public static void main(String[] args) {
		Pair2Triple p2t = new Pair2Triple("cassandra");
		p2t.getFilePairAmongTriple(30, 15);
	}
}
